import React, {useState} from 'react'

export default function TextForm(props) {
    const handleUpClick = () =>{
        // console.log("Uppercase was clicked" + text);
        let newText = text.toUpperCase();
        setText(newText)
        props.showAlert("converted to uppercase","success")
    }
    const handleOnChange = (event) =>{
        // console.log("On change");
        setText(event.target.value)
    }
    const handleLoClick = () =>{
        // console.log("Lowercase was clicked" + text);
        let newText = text.toLowerCase();
        setText(newText)
        props.showAlert("converted to lowercase","success")
    }
    const handleClearClick = () =>{
        // console.log("Cleartext was clicked" + text);
        let newText = '';
        setText(newText)
        props.showAlert("text cleared!","success")
    }
    const handleCapitalizedClick = () =>{
        // console.log("Capitalized was clicked" + text);
        let arr1 = text.toString().split(" ");
        let i; 
        for(i=0;i<arr1.length;i++){
            let newText = arr1[i].charAt(0).toUpperCase() + arr1[i].slice(1);
            arr1[i] = newText;
        } 
        let mm = arr1.toString().replace(/,/g," ");
        setText(mm)
        props.showAlert("converted to capitalizedform","success")
    }
    const handleCopyClick = () => {
        let text = document.getElementById("myBox")
        text.select();
        navigator.clipboard.writeText(text.value);
        props.showAlert("copy to clipboard","success")
    }
    const handleExtraSpacesClick = () => {
        let newText = text.split(/[  ]+/);
        setText(newText.join(" "))
        props.showAlert("extraspaces removed!","success")
    }
    const [text, setText] = useState('');
    // Text = "new Text"; // Wrong way to change the state
    // setText("new Text"); // Correct way to change the state
    return (
        <>
        <div className="container" style={{color: props.mode==='dark'?'white':'#042743'}}>
            <h1>{props.heading}</h1>
            <div className="mb-3">
            <textarea className="form-control" value={text} onChange={handleOnChange} id="myBox" rows="8" style={{backgroundColor: props.mode==='dark'?'grey':'white', color: props.mode==='dark'?'white':'#042743'}}></textarea>
            </div>
            <button className="btn btn-primary mx-1" onClick={handleUpClick}>Convert to Uppercase</button>
            <button className="btn btn-primary mx-1" onClick={handleLoClick}>Convert to Lowercase</button>
            <button className="btn btn-primary mx-1" onClick={handleClearClick}>Clear text</button>
            <button className="btn btn-primary mx-1" onClick={handleCapitalizedClick}>Convert to Capitalized</button>
            <button className="btn btn-primary mx-1" onClick={handleCopyClick}>Copy text</button>
            <button className="btn btn-primary mx-1" onClick={handleExtraSpacesClick}>Remove ExtraSpaces</button>
        </div>
        <div className="container my-3" style={{color: props.mode==='dark'?'white':'#042743'}}>
            <h2>Your text summary</h2>
            <p>{text.split(" ").filter((element)=>{return element.length!==0}).length} words, {text.length} characters</p>
            <p>{0.008 * text.split(" ").filter((element)=>{return element.length!==0}).length} Minutes read</p>
            <h2>Preview</h2>
            <p>{text.length>0?text:"Nothing to preview"}</p>
        </div>
        </>
    )
}
