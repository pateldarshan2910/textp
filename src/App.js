import "./App.css";
import About from "./Components/About";
import Navbar from "./Components/Navbar";
import TextForm from "./Components/TextForm";
import React, { useState } from 'react'
import Alert from "./Components/Alert";
import {
  BrowserRouter as Router,
  Routes,
  Route,
} from "react-router-dom";


function App() {
  const [mode,setMode] = useState('light'); // whether dark mode is enable or not
  const [alert,setAlert] = useState(null);

  const showAlert = (message, type) => {
      setAlert({
        msg: message,
        type:  type 
      })
      setTimeout(() => {
        setAlert(null);
      },3000);
  }
  const toggleMode = () => {
    if (mode === 'light'){
      setMode('dark')
      document.body.style.backgroundColor = '#042743';
      showAlert("Dark mode has been enabled", "success")
      // document.title="TextUtils - Dark Mode";
      
      // setInterval(()=> {  
      //   document.title="TextUtils is Amazing Mode";
      // },2000);
      // setInterval(()=> {  
      //   document.title="Install TextUtils Now";
      // },1500);
    }
    else{
      setMode('light')
      document.body.style.backgroundColor = 'white';
      showAlert("light mode has been enabled", "success")
      // document.title="TextUtils - light Mode";
    }
  }
  return (
    <>
        <Router>
          <Navbar title="TextUtils" aboutText="about TextUtils" mode={mode} toggleMode={toggleMode}/>
          <Alert alert={alert}/>
          <div className="container my-3">
            <Routes> 
              {/* /users --> Component-1
              /users/Home --> Component-2  that's why we use exact!*/}
              <Route exact path="/about" element={<About/>}>
              </Route> 
              <Route exact path="/" element={<TextForm showAlert={showAlert} heading="Enter the text to analyze" mode={mode}/>}> 
              </Route>
            </Routes>
          </div>
        </Router>
    </>
  );
}

export default App;
